/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Comprador;
import ec.edu.espol.model.PersonaVtaCmp;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author UserWin10
 */
public class VentanaCompradorFXMLController implements Initializable {
    
    PersonaVtaCmp persona;

    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
      
    
    @FXML
    private void ofertar(MouseEvent event) {
        try {
            FXMLLoader loader = App.loadFXMLoad("OfertarVehiculosFXML");
            App.setRoot(loader);
            OfertarVehiculosFXMLController controlador =loader.getController();
            controlador.setComprador(persona);
            controlador.recibeTipo(1);
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
        }
    }
    
    
     @FXML
    private void perfil(MouseEvent event) {
         try {
            FXMLLoader loader = App.loadFXMLoad("InformacionFXML");
            App.setRoot(loader);
            InformacionFXMLController controlador = loader.getController();
            controlador.recibeDatosText(persona, "Comprador");
            controlador.recibeTipo(1);
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
        }
    }

    @FXML
    private void salir(MouseEvent event) {
        try {
            FXMLLoader loader = App.loadFXMLoad("LoginFXML");
            App.setRoot(loader);            
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
            } 
    }
    
    @FXML
    public void recibePerfilC(PersonaVtaCmp _p) {
        persona = _p;
    }
    
}
