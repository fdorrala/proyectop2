/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.PersonaVtaCmp;
import ec.edu.espol.model.Vehiculo;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
/**
 * FXML Controller class
 *
 * @author UserWin10
 */
public class RegistroVehiculoFXMLController implements Initializable {
   
    private String tipoVehiculo;
    private String tipoMotor;
    private String tipoCombus;  
    private String tipoTransmi;
    private String año;
    private File file;
    
    //ArrayList<File> imagenes = new ArrayList<>();
    ArrayList<Vehiculo> lstVehiculos = Vehiculo.retornarListaVeh();
    int TipoVentana;
    PersonaVtaCmp persona;
    
    @FXML
    private TextField placa;
    @FXML
    private TextField marca;
    @FXML
    private TextField modelo;
    @FXML
    private TextField color;
    @FXML
    private TextField recorrido;
    @FXML
    private TextField precio;
    @FXML
    private TextField vidrios;
    @FXML
    private ComboBox combVehiculo;
    @FXML
    private ComboBox combMotor;
    @FXML
    private ComboBox combCombus;
    @FXML
    private ComboBox combTransmi;
    @FXML
    private ComboBox combAño;
   
    @FXML
    private Button subirFoto;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> listaVehiculo = FXCollections.observableArrayList("Auto","Camioneta","Moto","Camion");
        ObservableList<String> listaMotor = FXCollections.observableArrayList("Gasolina","Diésel","Híbrido");
        ObservableList<String> listaCombus = FXCollections.observableArrayList("Gasolina","Diésel");
        ObservableList<String> listaTransmi = FXCollections.observableArrayList("Manual","Automático");
        ObservableList<String> listaAño = FXCollections.observableArrayList("1980","1981","1982","1983","1984", "1985", "1986", "1987", "1988", "1989",
                                                                            "1990","1991","1992","1993","1994", "1995", "1996", "1997", "1998", "1999",
                                                                            "2000","2001","2002","2003","2004", "2005", "2006", "2007", "2008", "2009",
                                                                            "2010","2011","2012","2013","2014", "2015", "2016", "2017", "2018", "2019",
                                                                            "2020","2021","2022","2023","2024", "2025", "2026", "2027", "2028", "2029","2030");
        combVehiculo.setItems(listaVehiculo);
        combMotor.setItems(listaMotor);
        combCombus.setItems(listaCombus);
        combTransmi.setItems(listaTransmi);
        combAño.setItems(listaAño);
        
        placa.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[A-Z]{0,3}[0-9]{0,4}"))?change: null));
        marca.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[A-Za-z]*"))?change: null));
        modelo.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[A-Za-z]*"))?change: null));
        color.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[A-Za-z]*"))?change: null));
        recorrido.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[0-9]*"))?change: null));
        precio.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[0-9]*"))?change: null));
        vidrios.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[0-9]*"))?change: null));              
    }    
    
    @FXML
    private void selectTipo(ActionEvent event) {
        combVehiculo = (ComboBox)event.getSource();
        tipoVehiculo = (String)combVehiculo.getValue();
        
    }

    @FXML
    private void selecMotor(ActionEvent event) {
        combMotor = (ComboBox)event.getSource();
        tipoMotor = (String)combMotor.getValue();
    }

    @FXML
    private void selectCombust(ActionEvent event) {
        combCombus = (ComboBox)event.getSource();
        tipoCombus = (String)combCombus.getValue();
    }

    @FXML
    private void selectTransmi(ActionEvent event) {
        combTransmi = (ComboBox)event.getSource();
        tipoTransmi = (String)combTransmi.getValue();
    }

    @FXML
    private void selectaño(ActionEvent event) {
        combAño = (ComboBox)event.getSource();
        año = (String)combAño.getValue();
    }

    @FXML
    private void registrar(MouseEvent event) {
        String resp = validarCamposTexto();
        if(!(resp.equals(""))){
            Alert alert = new Alert(Alert.AlertType.WARNING, resp);
            alert.show();
       
       }else{        
            Vehiculo veh = initVehiculo();

            String msjExiste = Vehiculo.ExisteVehiculo(lstVehiculos, veh);
             if(!msjExiste.equals("")){
                    Alert alert = new Alert(Alert.AlertType.ERROR, msjExiste);
                    alert.show();            
                }else{
                    lstVehiculos.add(veh);
                    Vehiculo.guardarListaVeh(lstVehiculos);
                    file = null;
                    //Alert alert = new Alert(Alert.AlertType.INFORMATION, "Vehiculo registrado correctamente");
                    //alert.show(); 
                    Alert alert1 = new Alert(Alert.AlertType.CONFIRMATION);
                    alert1.setContentText("Desea registrar otro Vehiculo");
                    Optional<ButtonType> result = alert1.showAndWait();
                    if(result.get() == ButtonType.OK){
                        placa.setText("");
                        modelo.setText("");
                        marca.setText("");
                        color.setText("");
                        recorrido.setText("");
                        vidrios.setText("");
                        precio.setText("");

                    } else{
                        switch (TipoVentana) {
                            case 2:
                                try {
                                    FXMLLoader loader = App.loadFXMLoad("VentanaVendedorFXML");
                                    App.setRoot(loader);
                                    VentanaVendedorFXMLController controlador = loader.getController();
                                    controlador.recibePerfilV(persona);
                                }catch (IOException ex) {
                                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                                    alert.show();
                                }   break;
                            case 3:
                                 try {
                                    FXMLLoader loader = App.loadFXMLoad("VentanaVtaCmpFXML");
                                    App.setRoot(loader);
                                    VentanaVtaCmpFXMLController controlador = loader.getController();
                                    controlador.recibePerfilVC(persona);
                                }catch (IOException ex) {
                                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                                    alert.show();
                                }   break;                   
                            default:
                                break;
                        }
                    }
                }
        }
    }
    
    @FXML
    public void cargarFotos(ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("PNG", "*.png"), new FileChooser.ExtensionFilter("JPG", "*.jpg"));
       file = fc.showOpenDialog(subirFoto.getScene().getWindow());
        
    }

    @FXML
    private void salir(MouseEvent event) {
         switch (TipoVentana) {
            case 2:
                try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaVendedorFXML");
                    App.setRoot(loader);
                    VentanaVendedorFXMLController controlador = loader.getController();
                    controlador.recibePerfilV(persona);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;
            case 3:
                 try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaVtaCmpFXML");
                    App.setRoot(loader);
                    VentanaVtaCmpFXMLController controlador = loader.getController();
                    controlador.recibePerfilVC(persona);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;                   
            default:
                break;
        } 
    }
    
    private Vehiculo initVehiculo(){
        Vehiculo veh = new Vehiculo();
        
        veh.setTipo(tipoVehiculo);
        veh.setPlaca(placa.getText());
        veh.setMarca(marca.getText());
        veh.setModelo(modelo.getText());
        veh.setColor(color.getText());
        veh.setTipoMotor(tipoMotor);
        veh.setTipoCombustible(tipoCombus);
        veh.setTransmision(tipoTransmi);
        veh.setVidrios(vidrios.getText());
        veh.setAnio(Integer.parseInt(año));
        veh.setRecorrido(Integer.parseInt(recorrido.getText()));
        veh.setPrecio(Double.parseDouble(precio.getText()));
        veh.setImagen(file);
      
       
        return veh;
    }
    
     public String validarCamposTexto(){
        
        String msg = ""; 
        if(!(placa.getText().equals("") && marca.getText().equals("") && modelo.getText().equals("") &&  color.getText().equals("")
                && vidrios.getText().equals("") && recorrido.getText().equals("") && precio.getText().equals(""))){
            if(!(combVehiculo.equals("") && combMotor.equals("") && combCombus.equals("") && combTransmi.equals("") && combAño.equals(""))){
                return msg;
            }else{
                msg = "Selecciones Datos";
            }
        }else{
            msg = "Rellenar Datos";
        }

        return msg;
    }
    
     @FXML
    public void recibeTipo(int tipo){
        TipoVentana = tipo;
    }
   
    @FXML
    public void setUsuario(PersonaVtaCmp p){
        persona = p;
    }
}
