/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Comprador;
import ec.edu.espol.model.PersonaVtaCmp;
import ec.edu.espol.model.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
/**
 * FXML Controller class
 *
 * @author UserWin10
 */
public class InformacionFXMLController implements Initializable {
    
    ArrayList<Comprador> lstCompradores = Comprador.RetornarListaCBinario();
    ArrayList<Vendedor> lstVendedores = Vendedor.RetornarListaVBinario();
    PersonaVtaCmp persona;
    int TipoVentana;
    int indice;
    
    @FXML
    private Text nombres;
    @FXML
    private Text apellidos;
    @FXML
    private Text email;
    @FXML
    private Text organizacion;
    @FXML
    private Text usuario;
    @FXML
    private Text clave;
    @FXML
    private Text rol;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void CambiarClave(MouseEvent event) {
         try {
            FXMLLoader loader = App.loadFXMLoad("CambiarClaveFXML");
            App.setRoot(loader);
            CambiarClaveFXMLController controlador = loader.getController();
            controlador.recibePersona(persona, TipoVentana);            
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
        }
    }
    
    @FXML
    private void regresar(MouseEvent event) {
        switch (TipoVentana) {
            case 1:
                try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaCompradorFXML");
                    App.setRoot(loader);
                    VentanaCompradorFXMLController controlador = loader.getController();
                    controlador.recibePerfilC(persona);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;
            case 2:
                 try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaVendedorFXML");
                    App.setRoot(loader);
                    VentanaVendedorFXMLController controlador = loader.getController();
                    controlador.recibePerfilV(persona);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;               
            case 3:
                try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaVtaCmpFXML");
                    App.setRoot(loader);
                    VentanaVtaCmpFXMLController controlador = loader.getController();
                    controlador.recibePerfilVC(persona);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;   
                
            default:
                break;
        }    
    }
    
     @FXML
    private void CambiarRol(MouseEvent event) {
        switch (TipoVentana) {
            case 1:
                Alert alert1 = new Alert(Alert.AlertType.CONFIRMATION);
                alert1.setContentText("Desea registrar esta cuenta como Vendedor");
                Optional<ButtonType> result = alert1.showAndWait();
                if(result.get() == ButtonType.OK){
                    TipoVentana = 3;
                    rol.setText("Comprador/Vendedor"); 
                    Vendedor vend = initVendedor();
                    lstVendedores.add(vend);
                    Vendedor.guardarListVBinario(lstVendedores);   
                    System.out.println(nombres.getText());
                }                       
                break;  
                
            case 2:
                Alert alert2 = new Alert(Alert.AlertType.CONFIRMATION);
                alert2.setContentText("Desea registrar esta cuenta como Comprador");
                Optional<ButtonType> result2 = alert2.showAndWait();
                if(result2.get() == ButtonType.OK){
                    TipoVentana = 3;
                    rol.setText("Comprador/Vendedor"); 
                    Comprador cmp = initComprador();
                    lstCompradores.add(cmp);
                    Comprador.guardarListCBinario(lstCompradores);   
                    System.out.println(nombres.getText());
                }         
                break;               
            case 3:
                
                Alert alert3 = new Alert(Alert.AlertType.INFORMATION, "Usted ya es Comprador y Vendedor");
                alert3.show();
                break;   
                
            default:
                break;
        }       
    }
    
    @FXML
    public void recibeDatosText(PersonaVtaCmp p, String _rol) {
        persona = p;
        nombres.setText(p.getNombres());
        apellidos.setText(p.getApellidos());
        email.setText(p.getEmail());
        organizacion.setText(p.getOrganizacion());
        usuario.setText(p.getUsuario());
        clave.setText("*******");
        rol.setText(_rol);
    }
    
    @FXML
    public void recibeTipo(int tipo) {
        TipoVentana = tipo;
    }
    
    public Comprador initComprador(){
        
        Comprador cmp = new Comprador();
     
        cmp.setNombres(persona.getNombres());
        cmp.setApellidos(persona.getApellidos());
        cmp.setEmail(persona.getEmail());
        cmp.setOrganizacion(persona.getOrganizacion());
        cmp.setUsuario(persona.getUsuario());
        cmp.setClave(persona.getClave());
        
        return cmp;
    }
    
    public Vendedor initVendedor(){
        
        Vendedor vend = new Vendedor();
        
        vend.setNombres(persona.getNombres());
        vend.setApellidos(persona.getApellidos());
        vend.setEmail(persona.getEmail());
        vend.setOrganizacion(persona.getOrganizacion());
        vend.setUsuario(persona.getUsuario());
        vend.setClave(persona.getClave()); 
       
         return vend;
    }

}

