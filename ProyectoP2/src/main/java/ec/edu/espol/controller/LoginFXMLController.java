/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Comprador;
import ec.edu.espol.model.PersonaVtaCmp;
import ec.edu.espol.model.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author UserWin10
 */
public class LoginFXMLController implements Initializable {

    ArrayList<Comprador> lstCompradores = Comprador.RetornarListaCBinario();
    ArrayList<Vendedor> lstVendedores = Vendedor.RetornarListaVBinario();
    
    @FXML
    private TextField user;
    @FXML
    private PasswordField password;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void login(MouseEvent event) throws Exception {
        int TIPO_PERSONA = GetLogin(lstCompradores, lstVendedores);
        switch (TIPO_PERSONA) {
            case 1:
                try {
                    PersonaVtaCmp p = getPersona(lstCompradores);
                    FXMLLoader loader = App.loadFXMLoad("VentanaCompradorFXML");
                    App.setRoot(loader);
                    VentanaCompradorFXMLController controlador = loader.getController();
                    controlador.recibePerfilC(p);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;
            case 2:
                try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaVendedorFXML");
                    App.setRoot(loader);
                    PersonaVtaCmp p = getPersona(lstVendedores);
                    VentanaVendedorFXMLController controlador = loader.getController();
                    controlador.recibePerfilV(p);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;
            case 3:
                try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaVtaCmpFXML");
                    App.setRoot(loader);
                    PersonaVtaCmp p = getPersona(lstCompradores);
                    VentanaVtaCmpFXMLController controlador = loader.getController();
                    controlador.recibePerfilVC(p);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;
            default:
                Alert a = new Alert(Alert.AlertType.ERROR, "Usuario o contraseña incorrecta");
                a.show();
                break;
        }
    }

    @FXML
    private void registrarse(MouseEvent event) {
        try {
            FXMLLoader loader = App.loadFXMLoad("RegistroFXML");
            App.setRoot(loader);
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
        }
    }
    
    
    public int GetLogin(ArrayList<Comprador> lstC, ArrayList<Vendedor> lstV ) throws Exception{
        String usuario = user.getText();
        String clave = password.getText();

        return PersonaVtaCmp.ejecLogin(lstC,lstV, usuario, clave);

    }
    
    public PersonaVtaCmp getPersona(ArrayList personas) throws Exception{
        String usuario = user.getText();
        String clave = password.getText();

        return PersonaVtaCmp.GetPersona(personas, usuario, clave);

    }
}
