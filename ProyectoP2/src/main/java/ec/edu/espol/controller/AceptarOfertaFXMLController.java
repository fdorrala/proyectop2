
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.EnvioEmail;
import ec.edu.espol.model.OfertaCompra;
import ec.edu.espol.model.PersonaVtaCmp;
import ec.edu.espol.model.Vehiculo;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class AceptarOfertaFXMLController implements Initializable {


    @FXML
    private TextField placaVeh;
    @FXML
    private VBox vboxOfertas;
    
    private int indice=0;
    private ArrayList<OfertaCompra> ofertasPlaca;
    private ArrayList<OfertaCompra> ofertas;
    
    int TipoVentana;
    PersonaVtaCmp persona;
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //TODO
        placaVeh.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[A-Z]{0,3}[0-9]{0,4}"))?change: null));
    }    
    
    private void cambiar(){
        vboxOfertas.getChildren().clear(); 
        String dato="Se han realizado: "+ofertasPlaca.size()+" ofertas\n\n"+"Correo:  \t\t\t\t"+ofertasPlaca.get(indice).getOfertante().getEmail()+"\n"+
                "Precio Ofertado:\t\t\t"+ofertasPlaca.get(indice).getValor();
        Text texto=new Text(dato);
        vboxOfertas.getChildren().add(texto);
    }
    
    @FXML
    private void buscarOfertas(MouseEvent event) {
        String resp = validarPlaca();
        if(!(resp.equals(""))){
            Alert a=new Alert(Alert.AlertType.INFORMATION,resp);
            a.show();
        }else{
            ofertas=OfertaCompra.retornarListaOfertas();
            ofertasPlaca=OfertaCompra.GetOfertasxPlaca(ofertas, placaVeh.getText());
            if(ofertasPlaca!=null){
                cambiar();
                indice = 0;
            }else{
                Alert a=new Alert(Alert.AlertType.INFORMATION,"No existen ofertas");
                a.show();
            }        
        }
    }

    @FXML
    private void Anterior(MouseEvent event) {
        if((indice-1)<0){
            Alert a=new Alert(Alert.AlertType.INFORMATION,"No se puede retrocer ya que se está presentando la primera oferta");
            a.show();
        }
        else{
            indice--;
            cambiar();
        }         
    }

    @FXML
    private void Regresar(MouseEvent event) {
        switch (TipoVentana) {
            case 2:
                try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaVendedorFXML");
                    App.setRoot(loader);
                    VentanaVendedorFXMLController controlador = loader.getController();
                    controlador.recibePerfilV(persona);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;
            case 3:
                 try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaVtaCmpFXML");
                    App.setRoot(loader);
                    VentanaVtaCmpFXMLController controlador = loader.getController();
                    controlador.recibePerfilVC(persona);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;                   
            default:
                break;
        }      
    }

    @FXML
    private void Siguiente(MouseEvent event) {
        if((indice+1)>=ofertasPlaca.size()){
            Alert a=new Alert(Alert.AlertType.INFORMATION,"No se puede avanzar mas ya que no existen mas ofertas");
            a.show();
        }
        else{
            indice++;
            cambiar();
        }        
    }

    @FXML
    private void AceptarOferta(MouseEvent event) {
        ofertasPlaca.get(indice).setAceptada(true);
        EnvioEmail envio = new EnvioEmail("alguienespol@gmail.com", "Computadora123");
                        envio.enviarByGMail(ofertasPlaca.get(indice).getOfertante().getEmail(), 
                                ofertasPlaca.get(indice).generarAsuntoAcepta(), 
                                ofertasPlaca.get(indice).generarCuerpoMailAcepta());
        OfertaCompra.eliminarOferta(ofertasPlaca.get(indice).getVehOferta().getPlaca()); 
        Vehiculo.eliminarVehiculo(ofertasPlaca.get(indice).getVehOferta().getPlaca());
        vboxOfertas.getChildren().clear(); 
        placaVeh.setText("");
        Alert a=new Alert(Alert.AlertType.INFORMATION,"Oferta Aceptada");
        a.show();
    }
    
    private String validarPlaca(){
        String msg = "";
        if(!(placaVeh.getText().equals(""))){
            return msg;
        }else{
            msg = "Ingresar una Placa";
        } 
        return msg;
    }
    
     @FXML
    public void recibeTipo(int tipo){
        TipoVentana = tipo;
    }
   
    @FXML
    public void setUsuario(PersonaVtaCmp p){
        persona = p;
    }
    
    
    @FXML
    private void ordenarPrecio(MouseEvent event) {
        ofertas=OfertaCompra.retornarListaOfertas();
        ofertasPlaca=OfertaCompra.GetOfertasxPlaca(ofertas, placaVeh.getText());
        ofertasPlaca.sort(OfertaCompra::compareTo);
        indice=0;
        cambiar();
    }

    @FXML
    private void ordenarAnio(MouseEvent event) {
        ofertas=OfertaCompra.retornarListaOfertas();
        ofertasPlaca=OfertaCompra.GetOfertasxPlaca(ofertas, placaVeh.getText());
        ofertasPlaca.sort((OfertaCompra o1,OfertaCompra o2)->o1.getVehOferta().getAnio()-o2.getVehOferta().getAnio());
        indice=0;
        cambiar();
    }
}
