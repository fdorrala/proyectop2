/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.CodificadorSHA;
import ec.edu.espol.model.Comprador;
import ec.edu.espol.model.PersonaVtaCmp;
import ec.edu.espol.model.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author UserWin10
 */
public class CambiarClaveFXMLController implements Initializable {
    
    ArrayList<Comprador> lstCompradores = Comprador.RetornarListaCBinario();
    ArrayList<Vendedor> lstVendedores = Vendedor.RetornarListaVBinario();
    PersonaVtaCmp persona;
    int TipoVentana;
    
    
    @FXML
    private TextField claveActual;
    @FXML
    private TextField claveNueva;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void aceptar(MouseEvent event) {
        
        if(validarClave(claveActual.getText())){
            
                        
            switch (TipoVentana) {
            case 1:            
                
                Comprador cmp1 = initComprador(persona.getNombres(),persona.getApellidos(),persona.getEmail(),persona.getOrganizacion(),persona.getUsuario(),persona.getClave());
                lstCompradores.contains(persona);
                System.out.println(lstCompradores.contains(persona));
                persona.setClave(claveNueva.getText());
                persona.Codificar();
                Comprador cmp2 = initComprador(persona.getNombres(),persona.getApellidos(),persona.getEmail(),persona.getOrganizacion(),persona.getUsuario(),persona.getClave());               
                
                lstCompradores.add(cmp2);
                
                Comprador.guardarListCBinario(lstCompradores);
                
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "La clave ha sido cambiada");
                alert.show();
                
                break;
                
            case 2:
                        
                Vendedor ven1 = initVendedor(persona.getNombres(),persona.getApellidos(),persona.getEmail(),persona.getOrganizacion(),persona.getUsuario(),persona.getClave());
                persona.setClave(claveNueva.getText());
                persona.Codificar();
                Vendedor ven2 = initVendedor(persona.getNombres(),persona.getApellidos(),persona.getEmail(),persona.getOrganizacion(),persona.getUsuario(),persona.getClave());
                
                lstVendedores.remove(ven1);
                lstVendedores.add(ven2);
                
                Vendedor.guardarListVBinario(lstVendedores);
                
                Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "La clave ha sido cambiada");
                alert1.show(); 
                break;
                
            case 3:
                Comprador cmp_1 = initComprador(persona.getNombres(),persona.getApellidos(),persona.getEmail(),persona.getOrganizacion(),persona.getUsuario(),persona.getClave());
                               
                Vendedor ven_1 = initVendedor(persona.getNombres(),persona.getApellidos(),persona.getEmail(),persona.getOrganizacion(),persona.getUsuario(),persona.getClave());
                
                persona.setClave(claveNueva.getText());                
                persona.Codificar();
                
                Comprador cmp_2 = initComprador(persona.getNombres(),persona.getApellidos(),persona.getEmail(),persona.getOrganizacion(),persona.getUsuario(),persona.getClave());                
                
                Vendedor ven_2 = initVendedor(persona.getNombres(),persona.getApellidos(),persona.getEmail(),persona.getOrganizacion(),persona.getUsuario(),persona.getClave());
                
             
                lstCompradores.remove(cmp_1);
                lstVendedores.remove(ven_1);
                
                lstCompradores.add(cmp_2);
                lstVendedores.add(ven_2);                
                
                Comprador.guardarListCBinario(lstCompradores);
                Vendedor.guardarListVBinario(lstVendedores);
                
               
                Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "La clave ha sido cambiada");
                alert2.show(); 
                
                
                break;
                
            default:
                break;
            }

        }else{
             Alert alert = new Alert(Alert.AlertType.ERROR, "La clave actual no es la correcta");
             alert.show(); 
            }
          
    }

    @FXML
    private void cancelar(MouseEvent event) {
        try {
            FXMLLoader loader = App.loadFXMLoad("InformacionFXML");
            App.setRoot(loader);
            InformacionFXMLController controlador = loader.getController();
            String _rol = initRol();
            controlador.recibeDatosText(persona, _rol);
            controlador.recibeTipo(TipoVentana);
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
        }
    }
    
    @FXML
    public void recibePersona(PersonaVtaCmp p, int tipoVentana) {
        persona = p;
        TipoVentana = tipoVentana;
    }
    
     
    public String initRol(){
        String rol;
        switch (TipoVentana) {
            case 1:
                rol = "Comprador";
                return rol;
                
            case 2:
                rol = "Vendedor";
                return rol;
                
            case 3:
                rol = "Vendedor/Comprador";
                return rol;
                
            default:
                break;
        }
        return "";
    }
    
    public Comprador initComprador(String nombre, String apellidos, String email, String orga, String user, String passw){
        
        Comprador cmp = new Comprador();
     
        cmp.setNombres(nombre);
        cmp.setApellidos(apellidos);
        cmp.setEmail(email);
        cmp.setOrganizacion(orga);
        cmp.setUsuario(user);
        cmp.setClave(passw);
        
        return cmp;
    }
    
    public Vendedor initVendedor(String nombre, String apellidos, String email, String orga, String user, String passw){
        
        Vendedor vend = new Vendedor();
        
        vend.setNombres(nombre);
        vend.setApellidos(apellidos);
        vend.setEmail(email);
        vend.setOrganizacion(orga);
        vend.setUsuario(user);
        vend.setClave(passw); 
       
         return vend;
    }
    
    public boolean validarClave (String claveActual){
        
        CodificadorSHA codificador = new CodificadorSHA(claveActual);
            try {
                if(persona.getClave().equals(codificador.getCodificado()))
                   return true;

            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
            }
    
        return false;
    }

}