/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.PersonaVtaCmp;
import ec.edu.espol.model.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author UserWin10
 */
public class VentanaVendedorFXMLController implements Initializable {

    PersonaVtaCmp persona;
    
    @FXML
    private Button AceptOfert;
    
   
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    void ingresarVehiculo(MouseEvent event) {
        try {
            FXMLLoader loader = App.loadFXMLoad("RegistroVehiculoFXML");
            App.setRoot(loader);
            RegistroVehiculoFXMLController controlador = loader.getController();
            controlador.setUsuario(persona);
            controlador.recibeTipo(2);
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
            }
    }
    
    @FXML
    private void aceptarOferta(MouseEvent event) {
        try {
            FXMLLoader loader = App.loadFXMLoad("AceptarOfertaFXML");
            App.setRoot(loader);
            AceptarOfertaFXMLController controlador = loader.getController();
            controlador.recibeTipo(2);
            controlador.setUsuario(persona);
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
            }
    }
    
    @FXML
    private void perfil(MouseEvent event) {
        try {
            FXMLLoader loader = App.loadFXMLoad("InformacionFXML");
            App.setRoot(loader);
            InformacionFXMLController controlador = loader.getController();
            controlador.recibeDatosText(persona, "Vendedor");
            controlador.recibeTipo(2);
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
        }

    }
    
       @FXML
    private void salir(MouseEvent event) {
        try {
            FXMLLoader loader = App.loadFXMLoad("LoginFXML");
            App.setRoot(loader);
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
        } 
    }
    
    @FXML
    public void recibePerfilV(PersonaVtaCmp _p) {
        persona = _p;
    }

}

