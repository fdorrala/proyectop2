/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Comprador;
import ec.edu.espol.model.OfertaCompra;
import ec.edu.espol.model.PersonaVtaCmp;
import ec.edu.espol.model.Vehiculo;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author UserWin10
 */
public class OfertarVehiculosFXMLController implements Initializable {
    
    String tipoVeh;
    int _Rdesde = 0;
    int _Rhasta = 0;
    int _Adesde = 0;
    int _Ahasta = 0;
    double _Pdesde = 0 ;
    double _Phasta = 0;
    
    ArrayList<Vehiculo> lstVehiculo = Vehiculo.retornarListaVeh();    
    ArrayList<Vehiculo> vehFiltrado = new ArrayList<>();
    int indiceTemp = 0;
    
    PersonaVtaCmp persona;
    Comprador comprador;
    int TipoVentana;

    @FXML
    private TextArea textArea;
    @FXML
    private ComboBox cbxTipo;

    @FXML
    private TextField Rdesde;

    @FXML
    private TextField Rhasta;

    @FXML
    private TextField Adesde;

    @FXML
    private TextField Ahasta;

    @FXML
    private TextField Pdesde;

    @FXML
    private TextField Phasta;
    @FXML
    private TextField valor_oferta;

    @FXML
    private HBox hPane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> lista = FXCollections.observableArrayList("Auto","Camioneta","Moto","Camion");
        cbxTipo.setItems(lista);
       
        Rdesde.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[0-9]*"))?change: null));
        Rhasta.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[0-9]*"))?change: null));
        Adesde.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[0-9]*"))?change: null));
        Ahasta.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[0-9]*"))?change: null));
        Pdesde.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[0-9]*"))?change: null));
        Phasta.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[0-9]*"))?change: null));
        valor_oferta.setTextFormatter(new TextFormatter<>(change -> 
                (change.getControlNewText().matches("[0-9]*"))?change: null));
    }    
    
     @FXML
    private void ofertar(MouseEvent event) {        
        String resp = validarOferta();
        if(!(resp.equals(""))){
            Alert a=new Alert(Alert.AlertType.INFORMATION,resp);
            a.show();     
        }else{
            ArrayList<OfertaCompra> ofertas= OfertaCompra.retornarListaOfertas();
            OfertaCompra oferta = OfertaCompra.GetObtenerOferta(comprador,vehFiltrado.get(indiceTemp),Double.parseDouble(valor_oferta.getText()));
            ofertas.add(oferta);
            OfertaCompra.guardarOfertas(ofertas);
            Alert a=new Alert(Alert.AlertType.INFORMATION,"Su oferta ha sido registrada");
            a.show(); 
        } 
    }
    
    @FXML
    private void anterior(MouseEvent event) {
        if((indiceTemp-1)<0){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "No se puede retroceder");
        alert.show(); 
        }            
        else{
            indiceTemp--;
            textArea.clear();
            GetOpcionOfertaVehiculo(vehFiltrado.get(indiceTemp), indiceTemp);
        }
    }

    @FXML
    private void siguiente(MouseEvent event) {
        if((indiceTemp+1)>=vehFiltrado.size()){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "No se puede avanzar; no existen mas vehículos");
        alert.show(); 
        }            
        else{
            indiceTemp++;
            textArea.clear();
            GetOpcionOfertaVehiculo(vehFiltrado.get(indiceTemp), indiceTemp);
        }
    }
    
    
    @FXML
    private void buscarTipo(ActionEvent event) {
        cbxTipo = (ComboBox)event.getSource();
        tipoVeh = (String)cbxTipo.getValue();
    }

    @FXML
    private void filtrar(MouseEvent event) {
        switch (validarCamposTexto()) {
            case 1:
                textArea.clear(); 
                indiceTemp = 0;
                vehFiltrado = lstVehiculo;
                GetOpcionOfertaVehiculo(lstVehiculo.get(indiceTemp), indiceTemp);
                break;
            case 2:
                textArea.clear();
                indiceTemp = 0;
                vehFiltrado = Vehiculo.GetVehiculosxCriterios(lstVehiculo, tipoVeh, _Rdesde, _Rdesde, _Adesde, _Adesde, _Rdesde, _Rdesde);
                GetOpcionOfertaVehiculo(vehFiltrado.get(indiceTemp), indiceTemp);
                break;
            case 3:
                _Rdesde = Integer.parseInt(Rdesde.getText());
                _Rhasta = Integer.parseInt(Rhasta.getText());
                _Adesde = Integer.parseInt(Ahasta.getText());
                _Ahasta = Integer.parseInt(Ahasta.getText());
                _Pdesde = Integer.parseInt(Pdesde.getText());
                _Phasta = Integer.parseInt(Phasta.getText());
                indiceTemp = 0;
                vehFiltrado = Vehiculo.GetVehiculosxCriterios(lstVehiculo, tipoVeh, _Rdesde, _Rdesde, _Adesde, _Adesde, _Rdesde, _Rdesde);
                GetOpcionOfertaVehiculo(vehFiltrado.get(indiceTemp), indiceTemp);
                break;
            default:
                Alert alert = new Alert(Alert.AlertType.ERROR, "Fallo Filtro");
                alert.show(); 
                break;
        }  
    }
    
    @FXML
    private void regresar(MouseEvent event) {
        
        switch (TipoVentana) {
            case 1:
                try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaCompradorFXML");
                    App.setRoot(loader);
                    VentanaCompradorFXMLController controlador = loader.getController();
                    controlador.recibePerfilC(persona);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;
            case 3:
                 try {
                    FXMLLoader loader = App.loadFXMLoad("VentanaVtaCmpFXML");
                    App.setRoot(loader);
                    VentanaVtaCmpFXMLController controlador = loader.getController();
                    controlador.recibePerfilVC(persona);
                }catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                    alert.show();
                }   break;               
   
                
            default:
                break;
        }

    }
    
    private void GetOpcionOfertaVehiculo(Vehiculo vehCandidato, int indice){
        textArea.appendText("Tipo: " + vehCandidato.getTipo() + "\n");
        textArea.appendText("\n");
        textArea.appendText("Placa: " + vehCandidato.getPlaca() + "\n");
        textArea.appendText("Recorrido: " + vehCandidato.getRecorrido() + "km" +"\n");
        textArea.appendText("Año: " + vehCandidato.getAnio() + "\n");
        textArea.appendText("Precio: " + vehCandidato.getPrecio() + " USD" +"\n");
        textArea.appendText("\n");
        textArea.appendText("FICHA TECNICA " + "\n");
        textArea.appendText("\n");
        textArea.appendText("Marca: " + vehCandidato.getMarca() + "\n");
        textArea.appendText("Modelo: " + vehCandidato.getModelo() + "\n");
        textArea.appendText("Motor: " + vehCandidato.getTipoMotor() + "\n");
        textArea.appendText("Combustible: " + vehCandidato.getTipoCombustible() + "\n");
        textArea.appendText("Transmision: " + vehCandidato.getTransmision() + "\n");
        textArea.appendText("Color: " + vehCandidato.getColor() + "\n");
        textArea.appendText("Vidrios: " + vehCandidato.getVidrios() + "\n");
        
        hPane.getChildren().clear();
        hPane.setSpacing(10);
        
        File file=vehFiltrado.get(indiceTemp).getImagen();
        if(file != null){
            Image imagen = new Image(file.toURI().toString());
            ImageView imgMain = new ImageView(imagen);
            imgMain.setFitHeight(133);
            imgMain.setFitWidth(210);
            hPane.getChildren().add(imgMain);
        
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR, "No se encontro imagen");
            alert.show();
        
        }
        
    }
    
    public int validarCamposTexto(){
        
        int opc = -1; 
        if(tipoVeh != null){
            if(!(Rdesde.getText().equals("") && Rhasta.getText().equals("") && Adesde.getText().equals("") && Ahasta.getText().equals("") 
                    && Pdesde.getText().equals("") && Phasta.getText().equals(""))){
                opc = 3;
            }else{
                opc = 2;
            }
        }else{
            opc = 1;
        }

        return opc;
    }
    
    public String validarOferta(){
        String msg = "";
        if(!(valor_oferta.getText().equals(""))){
            return msg;
        }else{
            msg = "Hacer una oferta";
        } 
        return msg;
    }
    
     @FXML
    public void setComprador(PersonaVtaCmp p){
        persona = p;
        comprador=(Comprador)p;
    }
    
    @FXML
    public void recibeTipo(int tipo){
        TipoVentana = tipo;
    }
  
}
