/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Comprador;
import ec.edu.espol.model.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author UserWin10
 */
public class RegistroFXMLController implements Initializable {

    private int tipoPersona;

    ArrayList<Comprador> lstCompradores = Comprador.RetornarListaCBinario();
    ArrayList<Vendedor> lstVendedores = Vendedor.RetornarListaVBinario();
    
    @FXML
    private TextField nombres;
    @FXML
    private TextField apellidos;
    @FXML
    private TextField email;
    @FXML
    private TextField organizacion;
    @FXML
    private TextField user;
    @FXML
    private PasswordField password;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
    }    
    
    @FXML
    private void regresar(MouseEvent event) {
        try {
            FXMLLoader loader = App.loadFXMLoad("LoginFXML");
            App.setRoot(loader);            
        }catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
            alert.show();
        }  
    }

    @FXML
    private void regisComp(MouseEvent event) {
        String resp = validarCamposTexto();
        if(resp.equals("")){

            Comprador cmp = initComprador(); 
            Vendedor vend = initVendedor();

            String msjExisteC = Comprador.ExistePersonaComprador(lstCompradores, cmp);
            String msjExisteV = Vendedor.ExistePersonaVendedor(lstVendedores, vend);

            if(msjExisteC.equals("") && msjExisteV.equals("")){
                    cmp.Codificar();
                    lstCompradores.add(cmp);    
                    Comprador.guardarListCBinario(lstCompradores);
                    Alert alert1 = new Alert(Alert.AlertType.CONFIRMATION);
                    alert1.setContentText("Desea registrar otro usuario");
                    Optional<ButtonType> result = alert1.showAndWait();
                    if(result.get() == ButtonType.OK){
                        nombres.setText("");
                        apellidos.setText("");
                        email.setText("");
                        organizacion.setText("");
                        user.setText("");
                        password.setText("");
                    }else{
                         try {
                            FXMLLoader loader = App.loadFXMLoad("LoginFXML");
                            App.setRoot(loader);            
                        }catch (IOException ex) {
                            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                            alert.show();
                        }  
                    }
            }else{                    
                Alert alert = new Alert(Alert.AlertType.ERROR, "Correo electrónico o usuario registrados en el sistema");
                alert.show();
            }

        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING, resp);
            alert.show();  
        }
    }

    @FXML
    private void regisVend(MouseEvent event) {
        String resp = validarCamposTexto();
        if(resp.equals("")){

            Comprador cmp = initComprador(); 
            Vendedor vend = initVendedor();

            String msjExisteC = Comprador.ExistePersonaComprador(lstCompradores, cmp);
            String msjExisteV = Vendedor.ExistePersonaVendedor(lstVendedores, vend);

            if(msjExisteC.equals("") && msjExisteV.equals("")){
                    vend.Codificar();
                    lstVendedores.add(vend);    
                    Vendedor.guardarListVBinario(lstVendedores);
                    Alert alert1 = new Alert(Alert.AlertType.CONFIRMATION);
                    alert1.setContentText("Desea registrar otro usuario");
                    Optional<ButtonType> result = alert1.showAndWait();
                    if(result.get() == ButtonType.OK){
                        nombres.setText("");
                        apellidos.setText("");
                        email.setText("");
                        organizacion.setText("");
                        user.setText("");
                        password.setText("");
                    }else{
                         try {
                            FXMLLoader loader = App.loadFXMLoad("LoginFXML");
                            App.setRoot(loader);            
                        }catch (IOException ex) {
                            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                            alert.show();
                        }  
                    }                 
            }else{
                Alert alert = new Alert(Alert.AlertType.ERROR, "Correo electrónico o usuario registrados en el sistema");
                alert.show();                    
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING, resp);
            alert.show();  
        }
    }

    @FXML
    private void regisCmpVta(MouseEvent event) {
        String resp = validarCamposTexto();
        if(resp.equals("")){

            Comprador cmp = initComprador(); 
            Vendedor vend = initVendedor();

            String msjExisteC = Comprador.ExistePersonaComprador(lstCompradores, cmp);
            String msjExisteV = Vendedor.ExistePersonaVendedor(lstVendedores, vend);

            if(msjExisteC.equals("") && msjExisteV.equals("")){
                    cmp.Codificar();
                    vend.Codificar();
                    lstCompradores.add(cmp); 
                    lstVendedores.add(vend);
                    Comprador.guardarListCBinario(lstCompradores);
                    Vendedor.guardarListVBinario(lstVendedores);                       
                    Alert alert1 = new Alert(Alert.AlertType.CONFIRMATION);
                    alert1.setContentText("Desea registrar otro Usuario");
                    Optional<ButtonType> result = alert1.showAndWait();
                    if(result.get() == ButtonType.OK){
                        nombres.setText("");
                        apellidos.setText("");
                        email.setText("");
                        organizacion.setText("");
                        user.setText("");
                        password.setText("");
                    }else{
                         try {
                            FXMLLoader loader = App.loadFXMLoad("LoginFXML");
                            App.setRoot(loader);            
                        }catch (IOException ex) {
                            Alert alert = new Alert(Alert.AlertType.WARNING, "No se puedo cambiar la ventana");
                            alert.show();
                        }  
                    }
            }else{                    
                Alert alert = new Alert(Alert.AlertType.ERROR, "Correo electrónico o usuario registrados en el sistema");
                alert.show();
            }

        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING, resp);
            alert.show();  
        }
    }
    
    public Comprador initComprador(){
        
        Comprador cmp = new Comprador();
     
        cmp.setNombres(nombres.getText());
        cmp.setApellidos(apellidos.getText());
        cmp.setEmail(email.getText());
        cmp.setOrganizacion(organizacion.getText());
        cmp.setUsuario(user.getText());
        cmp.setClave(password.getText());
        
        return cmp;
    }
    
    public Vendedor initVendedor(){
        
        Vendedor vend = new Vendedor();
        
        vend.setNombres(nombres.getText());
        vend.setApellidos(apellidos.getText());
        vend.setEmail(email.getText());
        vend.setOrganizacion(organizacion.getText());
        vend.setUsuario(user.getText());
        vend.setClave(password.getText()); 
       
         return vend;
    }
    
    public boolean validarTextNomApell(String dato){
        return dato.matches("^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\\']+[\\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\\'])+[\\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\\'])?$");
    }
    
    public boolean validarTextEmail(String dato){
        return dato.matches("^(([^<>()\\[\\]\\\\.,;:\\s@”]+(\\.[^<>()\\[\\]\\\\.,;:\\s@”]+)*)|(“.+”))@((\\[[0–9]{1,3}\\.[0–9]{1,3}\\.[0–9]{1,3}\\.[0–9]{1,3}])|(([a-zA-Z\\-0–9]+\\.)+[a-zA-Z]{2,}))$");
    }
    
    
    public String validarCamposTexto(){
        
        String msg = ""; 
        if(!(organizacion.getText().equals("") && user.getText().equals("") && password.getText().equals(""))){
            if(validarTextNomApell(nombres.getText()) && validarTextNomApell(apellidos.getText())){
                if(validarTextEmail(email.getText())){
                    return msg;
                }else{
                    msg = "Email Incorrecto *[abc@mail.com]";
                }

            }else{
                msg = "Dos nombres/Dos Apellidos";        
            }
        }else{
            msg = "Rellenar Datos";
        }

        return msg;
    }

}