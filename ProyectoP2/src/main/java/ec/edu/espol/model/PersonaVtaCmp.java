
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class PersonaVtaCmp implements Serializable{

    public static final int TIPO_COMPRADOR = 1;
    public static final int TIPO_VENDEDOR = 2;
    
    protected String _nombres;
    protected String _apellidos;
    protected String _email;
    protected String _organizacion;
    protected String _usuario;
    protected String _clave;
    protected int _tipoPersona;

    public PersonaVtaCmp() {
        this._nombres = null;
        this._apellidos = null;
        this._email =  null;
        this._organizacion = null;
        this._usuario = null;
        this._clave = null;
    }   
    

    public PersonaVtaCmp(String _nombres, String _apellidos, String _email, String _organizacion, String _usuario, String _clave) {
        this._nombres = _nombres;
        this._apellidos = _apellidos;
        this._email = _email;
        this._organizacion = _organizacion;
        this._usuario = _usuario;
        this._clave = _clave;
        
    }
 
    
    public static boolean EjecLogin(ArrayList<PersonaVtaCmp> personas,String usuario, String clave, int tipo) throws Exception 
    {
        if(tipo==1){
         for(PersonaVtaCmp e: personas)
            {
                Comprador p = (Comprador)e;
                if(usuario.equals(p.getUsuario()))
                {
                    CodificadorSHA codificador = new CodificadorSHA(clave);
                    try {
                        if(p.getClave().equals(codificador.getCodificado()))
                           return true;

                    } catch (NoSuchAlgorithmException ex) {
                        throw new Exception("Error: Hubo un error al comparar las contraseñas.");
                    }
                }
            }    
        }
        else if(tipo == 2){
            for(PersonaVtaCmp e: personas)
            {
                Vendedor p = (Vendedor)e;
                if(usuario.equals(p._usuario))
                {
                    CodificadorSHA codificador = new CodificadorSHA(clave);
                    try {
                        if(p.getClave().equals(codificador.getCodificado()))
                           return true;

                    } catch (NoSuchAlgorithmException ex) {
                        throw new Exception("Error: Hubo un error al comparar las contraseñas.");
                    }
                }
            } 
        
        }

        return false;
    }
    
    
    
    
    public void Codificar()
    {
        CodificadorSHA codificador = new CodificadorSHA(this._clave);        
        try {
            this._clave = codificador.getCodificado();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void guardarListCBinario(ArrayList<Comprador> e){
        
        String ruta =  "Compradores.dat";
        try(ObjectOutputStream fichero = new ObjectOutputStream(new FileOutputStream(ruta))){
           
            fichero.writeObject(e);
    
        }
        catch (IOException ex) {
             System.out.println("No se ha encontrado archivo");
         }

    }

    public static ArrayList<Comprador> RetornarListaCBinario() {
       
        String ruta = "Compradores.dat";
        
        ArrayList<Comprador> lista = new ArrayList<>();
        
        try(ObjectInputStream recuperar_fichero = new ObjectInputStream(new FileInputStream(ruta))){
   
                 
            lista = (ArrayList)recuperar_fichero.readObject();
 
        } catch (Exception ex) {
            System.out.println("No se ha encontrado archivo o ha surgido un error al leer el archivo");
        }
    
        return lista;
    
    }
    
     public static void guardarListVBinario(ArrayList<Vendedor> e){
        
        String ruta =  "Vendedores.dat";
        try(ObjectOutputStream fichero = new ObjectOutputStream(new FileOutputStream(ruta))){
           
            fichero.writeObject(e);
    
        }
        catch (IOException ex) {
             System.out.println("No se ha encontrado archivo");
         }

    }
    
    public static ArrayList<Vendedor> RetornarListaVBinario() {
       
        String ruta = "Vendedores.dat";
        
        ArrayList<Vendedor> lista = new ArrayList<>();
        
        try(ObjectInputStream recuperar_fichero = new ObjectInputStream(new FileInputStream(ruta))){
   
                 
            lista = (ArrayList)recuperar_fichero.readObject();
 
        } catch (Exception ex) {
            System.out.println("No se ha encontrado archivo o ha surgido un error al leer el archivo");
        }
    
        return lista;
    
    }
    
    public static int ejecLogin(ArrayList<Comprador> listC, ArrayList<Vendedor> listV,String usuario, String clave) throws Exception
    {
        int x = 0;
        int y = 0;

        for(Comprador c: listC)
        {
            if(c.getUsuario().equals(usuario)){
                CodificadorSHA codificador = new CodificadorSHA(clave);
                try {
                    if(c.getClave().equals(codificador.getCodificado()))
                        x=1;                   
                }catch (NoSuchAlgorithmException ex) {
                    throw new Exception("Error: Hubo un error al comparar las contraseñas.");
                }    
            }
        }
        for(Vendedor v: listV)
        {
            if(v.getUsuario().equals(usuario)){
                CodificadorSHA codificador = new CodificadorSHA(clave);
                try {
                    if(v.getClave().equals(codificador.getCodificado()))
                        y=1;                   
                }catch (NoSuchAlgorithmException ex) {
                    throw new Exception("Error: Hubo un error al comparar las contraseñas.");
                }    
            }
        }
        
        if(x!=0 && y!=0)
            return 3;
        else{
            if(x!=0)
                return 1;
            else if(y!=0)
                return 2;
            else if(x==0 && y==0)
                return -1;
        }
   
        return -1;
    
    }
    
    public static PersonaVtaCmp GetPersona(ArrayList<PersonaVtaCmp> personas,String usuario, String clave) throws Exception 
    {
        for(PersonaVtaCmp p: personas){
            if(p.getUsuario().equals(usuario)){
                CodificadorSHA codificador = new CodificadorSHA(clave);
                try {
                    if(p.getClave().equals(codificador.getCodificado()))
                        return p;                   
                }catch (NoSuchAlgorithmException ex) {
                    throw new Exception("Error: Hubo un error al comparar las contraseñas.");
                }    
            }
        }
        
        return null;
    }
    
  
   
    public String getNombres() {
        return _nombres;
    }

    public void setNombres(String _nombres) {
        this._nombres = _nombres;
    }

    public String getApellidos() {
        return _apellidos;
    }

    public void setApellidos(String _apellidos) {
        this._apellidos = _apellidos;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String _email) {
        this._email = _email;
    }

    public String getOrganizacion() {
        return _organizacion;
    }

    public void setOrganizacion(String _organizacion) {
        this._organizacion = _organizacion;
    }

    public String getUsuario() {
        return _usuario;
    }

    public void setUsuario(String _usuario) {
        this._usuario = _usuario;
    }

    public String getClave() {
        return _clave;
    }

    public void setClave(String _clave) {
        this._clave = _clave;
    }    

    public int getTipoPersona() {
        return _tipoPersona;
    }
    
    
}
