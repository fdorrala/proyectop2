
package ec.edu.espol.model;



import java.util.ArrayList;


public class Vendedor extends PersonaVtaCmp{

    public Vendedor(String _nombres, String _apellidos, String _email, String _organizacion, String _usuario, String _clave) {
        super(_nombres, _apellidos, _email, _organizacion, _usuario, _clave);
    }

    public Vendedor() {
        
    }
    
    public static String ExistePersonaVendedor(ArrayList<Vendedor> vendedores, Vendedor buscada)
    {
        String msgOut = "";
        
        for(Vendedor c: vendedores)
        {
            if(c.getEmail().equals(buscada.getEmail())){
                msgOut = "El correo electrónico ya se encuentra registrado en el sistema"; 
                return msgOut;
            }else if(c.getUsuario().equals(buscada.getUsuario())){
                msgOut = "El usuario ya se encuentra registrado en el sistema"; 
                return msgOut;
            }
            
        }
        return msgOut;
    }
}
