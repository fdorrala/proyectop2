
package ec.edu.espol.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;


public class Vehiculo implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private final String TIPO_AUTO = "AUTO";
    private final String TIPO_CAMIONETA = "CAMIONETA";
    private final String TIPO_MOTO = "MOTO";
    private final String TIPO_CAMION = "CAMION";

    private String _tipo;
    private String _placa;
    private String _marca;
    private String _modelo;
    private String _tipoMotor;
    private int _anio;
    private int _recorrido;
    private String _color;
    private String _tipoCombustible;
    private String _vidrios;
    private String _transmision;
    private double _precio;
     private File imagen;

    public Vehiculo(String _tipo, String _placa, String _marca, String _modelo, String _tipoMotor, int _anio, int _recorrido, 
            String _color, String _tipoCombustible, String _vidrios, String _transmision, double _precio,  File imagen) {
        this._tipo = _tipo;
        this._placa = _placa;
        this._marca = _marca;
        this._modelo = _modelo;
        this._tipoMotor = _tipoMotor;
        this._anio = _anio;
        this._recorrido = _recorrido;
        this._color = _color;
        this._tipoCombustible = _tipoCombustible;
        this._vidrios = _vidrios;
        this._transmision = _transmision;
        this._precio = _precio;
        this.imagen=imagen;
        
    }

    public Vehiculo() {
        this._tipo = null;
        this._placa = null;
        this._marca = null;
        this._modelo = null;
        this._tipoMotor = null;
        this._anio = 0;
        this._recorrido = 0;
        this._color = null;
        this._tipoCombustible = null;
        this._vidrios = null;
        this._transmision = null;
        this._precio = 0;
        this.imagen= null;
    }
    
    public static String ExisteVehiculo(ArrayList<Vehiculo> vehiculos, Vehiculo buscada){
       String msgOut = "";
       
       for(Vehiculo v: vehiculos){
           if(v.getPlaca().equalsIgnoreCase(buscada.getPlaca()))
                {
                    msgOut = "El vehículo ya se encuentra registrado.";
                    return msgOut;
                }
       }
  
        return msgOut;
    }
    
    public static void guardarListaVeh(ArrayList<Vehiculo> e){
        
        String ruta =  "Vehiculos.dat";
        try(ObjectOutputStream fichero = new ObjectOutputStream(new FileOutputStream(ruta))){
           
            fichero.writeObject(e);
    
        }
        catch (IOException ex) {
             System.out.println("No se ha encontrado archivo");
         }

    }
    
    public static ArrayList<Vehiculo> retornarListaVeh() {
       
        String ruta = "Vehiculos.dat";
        
        ArrayList<Vehiculo> lista = new ArrayList<>();
        
        try(ObjectInputStream recuperar_fichero = new ObjectInputStream(new FileInputStream(ruta))){
     
            lista = (ArrayList)recuperar_fichero.readObject();
 
        } catch (Exception ex) {
            System.out.println("No se ha encontrado archivo o ha surgido un error al leer el archivo");
        }  
        return lista;
    }
    
    public static ArrayList<Vehiculo> GetVehiculosxCriterios(ArrayList<Vehiculo> vehiculos,String tipoVehiculo, int recorridoI, int recorridoF,
                                                int anioI, int anioF, double precioI, double precioF)
    {
        ArrayList<Vehiculo> vehiculosBusq = new ArrayList<>();
        
        for(int i=0;i<vehiculos.size();i++)
        {
            boolean incluir =true;
            Vehiculo v = vehiculos.get(i);
           
            incluir = incluir && v.getTipo().equalsIgnoreCase(tipoVehiculo);
            
            if(recorridoI>0)
            {
                incluir = incluir && v.getRecorrido()>=recorridoI;
            }
            if(recorridoF>0)
            {
                incluir = incluir && v.getRecorrido()<=recorridoF;
            }
            if(anioI>0)
            {
                incluir = incluir && v.getAnio()>=anioI;
            }
            if(anioF>0)
            {
                incluir = incluir && v.getAnio()<=anioF;
            }
            if(precioI>0)
            {
                incluir = incluir && v.getPrecio()>=precioI;   
            }
            if(precioF>0)
            {
                incluir = incluir && v.getPrecio()<=precioF;
            }
                
            if(incluir)
                vehiculosBusq.add(v);
        }
        
        return vehiculosBusq;
    }
    
    public static void eliminarVehiculo(String placa){
        ArrayList<Vehiculo> vehiculos = Vehiculo.retornarListaVeh();
        
        for(int i = 0;i<vehiculos.size();i++){
            Vehiculo temp = vehiculos.get(i);
            if(temp.getPlaca().equalsIgnoreCase(placa)){
               vehiculos.remove(temp);
            }
        } 
        Vehiculo.guardarListaVeh(vehiculos);
    }
    
    public String getTipo() {
        return _tipo;
    }

    public void setTipo(String tipo) {
        this._tipo = tipo;
    }

    public String getPlaca() {
        return _placa;
    }

    public void setPlaca(String _placa) {
        this._placa = _placa;
    }

    public String getMarca() {
        return _marca;
    }

    public void setMarca(String _marca) {
        this._marca = _marca;
    }

    public String getModelo() {
        return _modelo;
    }

    public void setModelo(String _modelo) {
        this._modelo = _modelo;
    }

    public String getTipoMotor() {
        return _tipoMotor;
    }

    public void setTipoMotor(String _tipoMotor) {
        this._tipoMotor = _tipoMotor;
    }

    public int getAnio() {
        return _anio;
    }

    public void setAnio(int _anio) {
        this._anio = _anio;
    }

    public int getRecorrido() {
        return _recorrido;
    }

    public void setRecorrido(int _recorrido) {
        this._recorrido = _recorrido;
    }

    public String getColor() {
        return _color;
    }

    public void setColor(String _color) {
        this._color = _color;
    }

    public String getTipoCombustible() {
        return _tipoCombustible;
    }

    public void setTipoCombustible(String _tipoCombustible) {
        this._tipoCombustible = _tipoCombustible;
    }

    public String getVidrios() {
        return _vidrios;
    }

    public void setVidrios(String _vidrios) {
        this._vidrios = _vidrios;
    }

    public String getTransmision() {
        return _transmision;
    }

    public void setTransmision(String _transmision) {
        this._transmision = _transmision;
    }

    public double getPrecio() {
        return _precio;
    }

    public void setPrecio(double _precio) {
        this._precio = _precio;
    }

    public File getImagen() {
        return imagen;
    }

    public void setImagen(File imagen) {
        this.imagen = imagen;
    }
}
