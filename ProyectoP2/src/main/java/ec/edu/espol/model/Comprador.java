/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;


import java.util.ArrayList;

/**
 *
 * @author UserWin10
 */
public class Comprador extends PersonaVtaCmp{
    
    
    
    public Comprador(String _nombres, String _apellidos, String _email, String _organizacion, String _usuario, String _clave) {
        super(_nombres, _apellidos, _email, _organizacion, _usuario, _clave);
    }

    public Comprador() {
       
    }
    
    public static String ExistePersonaComprador(ArrayList<Comprador> compradores, Comprador buscada)
    {
        String msgOut = "";
        
        for(Comprador c: compradores)
        {
            if(c.getEmail().equals(buscada.getEmail())){
                msgOut = "El correo electrónico ya se encuentra registrado en el sistema"; 
                return msgOut;
            }else if(c.getUsuario().equals(buscada.getUsuario())){
                msgOut = "El usuario ya se encuentra registrado en el sistema"; 
                return msgOut;
            }
            
        }
               
        return msgOut;
    } 
   
    
}
