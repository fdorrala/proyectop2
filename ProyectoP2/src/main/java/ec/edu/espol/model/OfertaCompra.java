/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author UserWin10
 */
public class OfertaCompra implements Serializable,Comparable<OfertaCompra>{
    private Comprador ofertante;
    private double valor;
    private boolean aceptada;
    private Vehiculo vehOferta;

    public OfertaCompra() {
    }
    public OfertaCompra(Comprador ofertante, double valor, boolean aceptada, Vehiculo vehOferta) {
        this.ofertante = ofertante;
        this.valor = valor;
        this.aceptada = aceptada;
        this.vehOferta = vehOferta;
    }

    public Comprador getOfertante() {
        return ofertante;
    }
    public void setOfertante(Comprador ofertante) {
        this.ofertante = ofertante;
    }
    public double getValor() {
        return valor;
    }
    public void setValor(double valor) {
        this.valor = valor;
    }
    public boolean isAceptada() {
        return aceptada;
    }
    public void setAceptada(boolean aceptada) {
        this.aceptada = aceptada;
    }
    public Vehiculo getVehOferta() {
        return vehOferta;
    }
    public void setVehOferta(Vehiculo vehOferta) {
        this.vehOferta = vehOferta;
    }
    
    public static void guardarOfertas(ArrayList<OfertaCompra> ofertas){
        try(ObjectOutputStream fichero = new ObjectOutputStream(new FileOutputStream("Ofertas.dat"))){
            fichero.writeObject(ofertas);
        } catch (IOException ex) {
            System.out.println("No se ha encontrado archivo o ha surgido un error al leer el archivo");
        }
    }
    
     public static void eliminarOferta(String placa){
        ArrayList<OfertaCompra> ofertas = OfertaCompra.retornarListaOfertas();
        
        for(int i = 0;i<ofertas.size();i++){
            OfertaCompra temp = ofertas.get(i);
            if(temp.getVehOferta().getPlaca().equalsIgnoreCase(placa)){
               ofertas.remove(temp);
            }
        } 
        OfertaCompra.guardarOfertas(ofertas);
    }
    
    public static ArrayList<OfertaCompra> retornarListaOfertas() {
        ArrayList<OfertaCompra> lista = new ArrayList<>();
        try(ObjectInputStream recuperar_fichero = new ObjectInputStream(new FileInputStream("Ofertas.dat"))){ 
            lista = (ArrayList<OfertaCompra>)recuperar_fichero.readObject();
        } catch (Exception ex) {
            System.out.println("No se ha encontrado archivo o ha surgido un error al leer el archivo");
        }
        return lista;
    }
    
    public static ArrayList<OfertaCompra> GetOfertasxPlaca(ArrayList<OfertaCompra> ofertas, String placa){
        ArrayList<OfertaCompra> ofertasPlaca = null; 
        for(int i = 0;i<ofertas.size();i++){
           OfertaCompra temp = ofertas.get(i);
           if(temp.getVehOferta().getPlaca().equalsIgnoreCase(placa)&& !temp.aceptada){
               if(ofertasPlaca==null)
                   ofertasPlaca = new ArrayList<>();
               ofertasPlaca.add(temp);
           }
       }
       return ofertasPlaca;
    }
    
    public String generarAsuntoAcepta(){
        String asunto;
        asunto = "Aceptación de oferta vehiculo " + this.getVehOferta().getMarca() + " " +this.getVehOferta().getModelo();        
        return asunto;
    }
    public String generarCuerpoMailAcepta(){
        String bodyMail = "";
        bodyMail = "Se ha aceptado la oferta del vehículo:\n";
        bodyMail += "Tipo Vehiculo: " + this.getVehOferta().getTipo() + "\n";
        bodyMail += "Placa: " + this.getVehOferta().getPlaca()+ "\n";
        bodyMail += "Modelo: " + this.getVehOferta().getModelo()+ "\n";
        bodyMail += "Precio: $" + this.getVehOferta().getPrecio()+ "\n";
        bodyMail += "Valor ofertado: $" + this.valor + "\n";
        return bodyMail;
    }
    
    public static String[] GetCriteriosOfertas(String tipoVehiculo,String primRecorrido,String segRecorrido,String primAño,String segAño,String primPrecio,String segPrecio){
        String[] criterios = new String[7];
        criterios[0] = tipoVehiculo;
        criterios[1] = primRecorrido;
        criterios[2] = segRecorrido;
        criterios[3] =primAño;
        criterios[4] = segAño;
        criterios[5] =primPrecio;
        criterios[6] = segPrecio;
        return criterios;
    }
    public static OfertaCompra GetObtenerOferta(Comprador personaCompra,Vehiculo veh,double valor){
        OfertaCompra oferta = new OfertaCompra();
        oferta.setValor(valor);
        oferta.setAceptada(false);
        oferta.setOfertante(personaCompra);
        oferta.setVehOferta(veh);
        return oferta;
    }
    
    @Override
    public int compareTo(OfertaCompra otraOferta) {
       if(this.getValor()>otraOferta.getValor()){
           return -1;
       }
       else if(this.getValor()==otraOferta.getValor()){
           return 0;
       }
       else{
           return 1;
       }
    }
}


    

